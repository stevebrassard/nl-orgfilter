import ko from 'knockout';
export default class DropdownOption {
    constructor(key, value, text) {
        this.key = key;
        this.value = value;
        this.text = text;
        this.isActive = ko.observable(false);
    }
}