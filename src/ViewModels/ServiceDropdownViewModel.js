import ko from 'knockout';
import postbox from 'knockout-postbox';
import DropdownOption from '../BusinessObjects/DropdownOption';
import {debounce} from '../Common/Utilities';
import DropdownFilterViewModel from '../ViewModels/DropdownFilterViewModel'; 

export default class DropdownServiceViewModel extends DropdownFilterViewModel  {
    constructor(props) {
        super(props);
        this.props = props;
        this.service = props.datasource.remote; 
        this.page = 1;
        this.cancelScrollRequest = false;

        if(this.props.datasource.remote) {            
            var ajaxOptions = {
                url: this.urlBuilder(this.service.url, null, 1, this.service.pageSize, this.service.mapping.textField, 'asc')
            };    
            this.fetch(ajaxOptions);          
        }
        else {
            console.log("The datasource property type was not recognized.")
        }       
        this.scrolled = debounce(this.scrolled, 100);  
        this.executeSearch = debounce(this.executeSearch, 200); 
    }

    urlBuilder(serviceUrl, query, page, limit, sort, order) {
        var parameters = [], url = `${serviceUrl}?`, p = null;
        if(query) parameters.push(`q=${query}`);
        if(page) parameters.push(`_page=${page}`);        
        if(limit) parameters.push(`_limit=${limit}`);
        if(sort) parameters.push(`sort=${sort}`);
        if(order) parameters.push(`order=${order}`);
        for(p in parameters) {
            url = url.concat(`${parameters[p]}&`);
        }
        return url.substring(0, url.length - 1);
    }

    executeSearch(query) 
    {
        console.log(`(Service) Search TextFilter Executed: ${query}`);
        this.query = query;  
        this.page = 1;      
        this.cancelScrollRequest = false;
        var ajaxOptions = {
            url: this.urlBuilder(this.service.url, this.query, 1, this.service.pageSize, this.service.mapping.textField, 'asc')
        }; 
        this.fetch(ajaxOptions);   
    }

    selectItem(item, event) {
        if(this.selectedItem()!==null) { 
            //clear previous selected item
            this.options()[this.selectedItem().key] ? this.options()[this.selectedItem().key].isActive(false) : this.selectedItem().isActive(false);
        };
        item.isActive(true);
        this.selectedItem(item);
        this.searchText.exclusiveUpdate(item.text, this);        
    }

    clearSelection(item, event) {
        if(this.selectedItem()!==null) {
            //clear previous selected item
            this.options()[this.selectedItem().key] ? this.options()[this.selectedItem().key].isActive(false) : this.selectedItem().isActive(false);             
        } 
        if(this.query) {
            var ajaxOptions = {
                url: this.urlBuilder(this.service.url, null, 1, this.service.pageSize, this.service.mapping.textField, 'asc')
            };
            this.query = null; 
            this.page = 1;              
            this.fetch(ajaxOptions);
        }   
        else {
            this.resetScrollPosition();
        } 
        this.selectedItem(null);    
        this.searchText.exclusiveUpdate("", this); 
        this.cancelScrollRequest = false;
        event.stopPropagation();
        event.preventDefault();            
        event.stopImmediatePropagation();
        return false;              
    }

    fetch(ajaxOptions) { 
        //Update ajaxDefaults to provide beforeSend / complete implementations for providing UI feedback for long requests (use pace.js)
        let ajaxDefaults = {
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",  
            //beforeSend: function() {},
            //complete: function() {},            
            context: this          
        }
        var ajaxOptions = $.extend(true, {}, ajaxOptions, ajaxDefaults);
        var searchPromise = $.ajax(ajaxOptions);
        searchPromise.done(function(response) {
            var keyStart = 0;
            if(!response.length && this.page>1) {               
                this.cancelScrollRequest = true;
                return;
            }
            if(this.service.mapping.dataWrapper) response = response[this.service.mapping.dataWrapper];
            if(this.page !== 1) keyStart = this.options().length;
            let results = response.map( (data, key) =>  {
                return new DropdownOption(key + keyStart, data[this.service.mapping.valueField], data[this.service.mapping.textField]);
            });
            this.dataset = results; 
            (this.page===1) ? this.options(this.dataset) : ko.utils.arrayPushAll(this.options, this.dataset); 
        });           
    }

    scrolled(data, event) {
        let elem = $(event.target);
        if (elem.scrollTop() > (elem[0].scrollHeight - elem[0].offsetHeight - 40)) { 
            this.page++;
            var ajaxOptions = {
                url: this.urlBuilder(this.service.url, this.query, this.page, this.service.pageSize, this.service.mapping.textField, 'asc'),
            };    
            if(!this.cancelScrollRequest) {               
                this.fetch(ajaxOptions);
            }
        }   
    }
};