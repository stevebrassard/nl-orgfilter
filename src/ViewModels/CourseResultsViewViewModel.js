import ko from 'knockout';
import postbox from 'knockout-postbox';

export default class CourseResultsViewViewModel {
    constructor(props) {
        //debugger;
        this.props = props;
        //this.results = props.data;
    }

    /* Create an implementation for working with the course results data */

    onSelectedName() {
        //debugger;
        console.log("CourseResultsViewViewModel->onSelectedName()", this);
    }

    onSelectedTitle() {
        //debugger;
        console.log("CourseResultsViewViewModel->onSelectedTitle()", this);
    }

}