import ko from 'knockout';
import {findObject} from '../Common/Utilities';

export default class SearchViewModel {
    constructor(props) {
        this.props = props;
        //this.template = ko.observable(props.templateName);
        this.results = ko.observable();
        this.fetch();
    }

    fetch() {
        let response = this.getData();
        let resultData = this.getMappedData(response);
        this.results(resultData);
    }

    getMappedData(response) {
        //Dynamically map response data to datasource mapping object properties (if mapping is required)
        try
        {            
            let mapping = this.props.datasource.remote.data.mapping; //if props doesnt have datasource.remote.mapping, exception occurs
            if(this.props.datasource.remote.data.dataWrapper) {
                response = findObject(response, this.props.datasource.remote.data.dataWrapper); //unwrap data if response contains a wrapped response. (ex. response.d.data)               
            }
            if(mapping) {
                response = response.map( (data, key) =>  {              
                    var mappedObject = new Object();
                    for (var key in mapping) {
                        if (mapping.hasOwnProperty(key)) {
                            mappedObject[key] = data[mapping[key]]; //for each key in datasource mapping, get the value from the mapping found in the response data
                        }
                    }
                    return mappedObject;            
                });  
            };
            return response;            
        }   
        catch(ex) {
            return response; //return response since no mapping exists
        }    
    }

    getData() {
        //simulates the data from a service call

        var results = [
            {
                "id": 1,
                "courseName": "United States",
                "courseTitle": "Black And Decker 3.6 Volt Lithium Ion Screwdriver, Li3100",
                "completedDate": "06/06/2006"
            },
                {
                "id": 2,
                "courseName": "United States",
                "courseTitle": "Pentagon Professional Aluminum Drywall Bench",
                "completedDate": "04/05/2013"
            },
                {
                "id": 3,
                "courseName": "United States",
                "courseTitle": "Buffalo Tools Pro Series 12 Piece Router Bit Set",
                "completedDate": "03/26/2010"
            },
                {
                "id": 4,
                "courseName": "United States",
                "courseTitle": "Stalwart 8-in-1 Multipurpose Lighted Magnetic Driver With Bits",
                "completedDate": "12/28/2007"
            },
                {
                "id": 5,
                "courseName": "United States",
                "courseTitle": "Dewalt 115-dw059b 18v 1-2 Inch High Torque Impact Wrench Bare Tool",
                "completedDate": "01/18/2001"
            },
                {
                "id": 6,
                "courseName": "United States",
                "courseTitle": "Dremel Workstation, 220-01",
                "completedDate": "07/23/2010"
            },
                {
                "id": 7,
                "courseName": "United States",
                "courseTitle": "Bostitch Industrial Air Tool Plug, 1/4 Npt, Btfp72318",
                "completedDate": "02/02/2014"
            },
                {
                "id": 8,
                "courseName": "United States",
                "courseTitle": "Fry Technologies Cookson Elect Am33505 50/50 Solder Solid Wire",
                "completedDate": "06/26/2001"
            },
                {
                "id": 9,
                "courseName": "United States",
                "courseTitle": "Titan Professional Workbench",
                "completedDate": "01/27/2010"
            },
                {
                "id": 10,
                "courseName": "United States",
                "courseTitle": "Homak 2 Door Mobile Cabinet With Shelves",
                "completedDate": "09/16/2003"
            },
                {
                "id": 11,
                "courseName": "United States",
                "courseTitle": "Eclipse Pz3x1-15/16 Bit Pozidrive Size 3 1-15/16in Long 1/4in Hex",
                "completedDate": "01/19/2003"
            },
                {
                "id": 12,
                "courseName": "United States",
                "courseTitle": "Lumberjack Tools Ttr0500 1/2in Pro Series Tenon Cutter",
                "completedDate": "09/16/2011"
            },
                {
                "id": 13,
                "courseName": "United States",
                "courseTitle": "Kd Tools Engraver Electric Vibro",
                "completedDate": "01/26/2002"
            },
                {
                "id": 14,
                "courseName": "United States",
                "courseTitle": "Dremel 3000 Rotary Tool",
                "completedDate": "01/02/2011"
            },
                {
                "id": 15,
                "courseName": "United States",
                "courseTitle": "Wagner Heat Gun Kit, Ht1100",
                "completedDate": "12/12/2015"
            },
                {
                "id": 16,
                "courseName": "United States",
                "courseTitle": "Black & Decker 20v Smart Select Cordless Drill",
                "completedDate": "06/26/2016"
            },
                {
                "id": 17,
                "courseName": "United States",
                "courseTitle": "Bostitch 1-1/2 Air Pressure Gauge, 1/4 Or 1/8 Npt(m), Btfp72328",
                "completedDate": "09/07/2010"
            },
                {
                "id": 18,
                "courseName": "United States",
                "courseTitle": "Pro-lift I-4506 6in Buffer / Polisher",
                "completedDate": "08/24/2016"
            },
                {
                "id": 19,
                "courseName": "United States",
                "courseTitle": "Black & Decker 6.0 Amp Accu-trak Saw With Smartselect, Scs600",
                "completedDate": "07/20/2000"
            },
                {
                "id": 20,
                "courseName": "United States",
                "courseTitle": "Black & Decker 1/4-sheet Sander With Filtered Dust Collection",
                "completedDate": "11/13/2004"
            },
                {
                "id": 21,
                "courseName": "United States",
                "courseTitle": "Rubbermaid Wall Washer Replacement Pads, White Rcps299",
                "completedDate": "10/26/2000"
            },
                {
                "id": 22,
                "courseName": "United States",
                "courseTitle": "Bosch Cs5 7-1/4\" 15-amp Circular Saw",
                "completedDate": "05/20/2017"
            },
                {
                "id": 23,
                "courseName": "United States",
                "courseTitle": "Lumberjack Tools Hsk2 Home Series 2-piece Starter Kit",
                "completedDate": "10/11/2004"
            },
                {
                "id": 24,
                "courseName": "United States",
                "courseTitle": "Dremel 3000 Rotary Tool",
                "completedDate": "06/27/2000"
            },
                {
                "id": 25,
                "courseName": "United States",
                "courseTitle": "Skil 360 Quick-select 4v Max 1.5ah Lithium-ion Screwdriver, 2356-01",
                "completedDate": "06/27/2010"
            }
        ];

       
        var response = {
            d: {
                data: results
            }
        }    
        
        return response;
    }
}
