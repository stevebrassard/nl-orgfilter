import ko from 'knockout';
import postbox from 'knockout-postbox';
import DropdownOption from '../BusinessObjects/DropdownOption';
import { debounce } from '../Common/Utilities';
import { DatasourceException } from '../Common/Exceptions';

export default class DropdownFilterViewModel {
    constructor(props) {
        this.props = props;
        this.placeholder = props.placeholder;
        this.readOnly = props.readOnly || false;
        this.disabled = props.disabled || false; 
        this.explicitUpdate = false;               
        this.selectedItem = ko.observable(null);
        this.options = ko.observableArray();          
        this.searchText = ko.observable(); //.extend({rateLimit: 600});
        this.searchText.subscribe((function(value, args) { 
            //user typed something as a query, so remove the previous selected item
            if(this.selectedItem() !== null && this.searchText() !== this.selectedItem().text) {
                this.dataset[this.selectedItem().key].isActive(false); //Remove active selected item 
                this.selectedItem(null);                
            }         
            if(this.explicitUpdate) { //dont call the search if its not nessecary
                this.explicitUpdate = false;
                return;
            };
            this.executeSearch(value);
        }).bind(this));
               
        // this.selectedItem.subscribe((function(value) {
        //     ko.postbox.publish(`${this.props.element.id}.OnSelected`, value);
        // }).bind(this))
     
        if(props.datasource) {
            if (this.props.datasource instanceof Array) {
                this.dataset = props.datasource.sort().map( (item, index) => {
                    if(typeof item === 'string') return new DropdownOption(index, index, item);
                    if(item instanceof Object) return new DropdownOption(index, item.value, item.text);                                
                });
                this.options(this.dataset);                
            }
        } 
        
        this.selectItem = this.selectItem.bind(this);
        this.executeSearch = debounce(this.executeSearch, 300);
        this.attachEventHandlers();
        this.registerBindingHandlers();
    }

    executeSearch(value) {
        console.log(`Search TextFilter Executed: ${value}`);          
        if(value.length) {
            const matched = this.dataset.filter( (item) => {
                return item.text.toLowerCase().indexOf(value.toLowerCase()) != -1;             
            });
            if(matched.length) {
                this.options(matched);
            }
            return;
        }
        this.options(this.dataset);
    }

    selectItem(item, event) {
        if(this.selectedItem()!==null) { 
            this.options()[this.selectedItem().key].isActive(false) 
        }; //get previous selected item and set it inactive  

        item.isActive(true);
        this.selectedItem(item);
        this.searchText.exclusiveUpdate(item.text, this);        
        this.options(this.dataset);
    }

    clearSelection(item, event) {
        if(this.selectedItem()!==null) {
            if(this.selectedItem()!==null) {                 
                this.options()[this.selectedItem().key].isActive(false) 
            }; //get previous selected item and set it inactive

            this.selectedItem(null);    
            this.searchText.exclusiveUpdate("", this);
            this.resetScrollPosition();
            event.stopPropagation();
            event.preventDefault();    
        }
    }

    attachEventHandlers() {
        //If a selection is not made, and user types in the input, clear the input text on menu close
        $(this.props.element).on('hide.bs.dropdown', (function(e) {              
            //If a selection is not made, and the input has text on close of dropdown-menu, reset the input to empty
            if(this.selectedItem() === null && !!this.searchText()) {
                this.searchText(""); 
                this.resetScrollPosition();                               
            }            
        }).bind(this));     

        //If a selection is not made, and user types in the input, clear the input text on menu close
        $(this.props.element).on('show.bs.dropdown', (function(e) {              
            //If a selection is not made, and the input has text on close of dropdown-menu, reset the input to empty
            if(this.selectedItem() === null) {
                this.resetScrollPosition();
            }            
        }).bind(this));     
        
        //Override the default bootstrap "toggle" class behavior when we have the menu open
        $(this.props.element).find("input.combobox").on('click focus', (function(e) {
            //find other open dropdowns, and close them
            $(".dropdown-menu:visible").each(function(index) {
                $(this).parent().removeClass("open");
            });            
            let dropdownMenu = $(e.currentTarget).parent().children(".dropdown-menu");
            if(dropdownMenu.is(":visible")) {
                dropdownMenu.parent().addClass("open");
            }

            //Check for selectedItem and scroll to that element in the results
            if(this.selectedItem() !== null ) {
                let scrollPosition = $(this.props.element).find(".active");
                if(scrollPosition.length) {
                    scrollPosition[0].scrollIntoView();
                }                            
            } 
            e.cancelBubble=true;
            e.stopPropagation();
            e.preventDefault();    
            e.stopImmediatePropagation();  
        }).bind(this));

        //Enable keyboard navigation
        $(this.props.element).find('.input-group input').keyup((function(e){                           
            if(e.which == 46) { // delete          
                if(this.selectedItem() !== null && this.searchText() !== this.selectedItem().text) {
                    this.clearSelection(null, e); 
                }
            }
            let input = $(this.props.element).find("input.combobox")[0];
            if(e.which == 8 && !input.value) { //backspace (while clearing out input)
                this.clearSelection(null, e);
            }    
        }).bind(this));

        $(this.props.element).find('.input-group input').keydown(function(e){
            if(e.which == 9 || e.which == 40){ // tab || down_arrow
                e.preventDefault();
                $(this).parent().find('.dropdown-toggle').click();
                $(this).parent().find('.dropdown-menu a:first').focus();
            }            
        });

        $(this.props.element).find('.dropdown-menu a').keydown(function(e){
            switch(e.which){
                case 36: // home
                e.preventDefault();
                $(this).closest('.dropdown-menu').find('a:first').focus();
            break;
                case 35: // end
                e.preventDefault();
                $(this).closest('.dropdown-menu').find('a:last').focus();
            break;
        }
        });         
    }

    resetScrollPosition() {
        //Hack to set scroll position if the element is hidden       
        let dropdown = $(this.props.element).find(".dropdown-menu");
        dropdown.css({visibility: 'visible', display: 'block'});
        dropdown.scrollTop(0);
        dropdown.css({visibility: '', display: ''}); 
    }

    registerBindingHandlers() {            
        ko.observable.fn.exclusiveUpdate = function(value, context) {                            
            context.explicitUpdate = true;
            this(value);
            this.notifySubscribers = function() {
                ko.subscribable.fn.notifySubscribers.apply(this, arguments);
            };            
        };          
    }

    scrolled(data, event) {} //do nothing for scroll events, other controls may use this feature
}   