import ko from 'knockout';
import PageViewModel from './ViewModels/PageViewModel';
import DropdownFilterComponent from './Components/DropdownFilterComponent';
import ServiceDropdownComponent from './Components/ServiceDropdownComponent';
import OrganizationFilterComponent from './Components/OrganizationFilterComponent';
import SearchComponent from './Components/SearchComponent';
import CourseResultsViewComponent from './Components/CourseResultsViewComponent';

//Ensure that the DOM is ready prior to applying binding UI
// var interval = setInterval(function() {
//     if ( document.readyState !== 'complete' ) return;
//     clearInterval(interval);
//     //console.log("ko.applyBindings");
//     ko.applyBindings(new PageViewModel(), document.getElementById('root')); //represents some VM the componets belong to
// }, 200);

ko.applyBindings(new PageViewModel(), document.getElementById('root')); //represents some VM the componets belong to