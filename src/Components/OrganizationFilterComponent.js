import ko from 'knockout';
import OrganizationFilterViewModel from '../ViewModels/OrganizationFilterViewModel';
import OrganizationFilterTemplate from '../Templates/OrganizationFilterTemplate.html';

export default ko.components.register("organization-filter", {
    viewModel: OrganizationFilterViewModel,
    template: OrganizationFilterTemplate
});
    