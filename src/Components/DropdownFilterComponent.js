import ko from 'knockout';
import DropdownFilterViewModel from '../ViewModels/DropdownFilterViewModel';
import DropdownFilterTemplate from '../Templates/DropdownFilterTemplate.html';
import '../Styles/DropdownFilter.css'

export default ko.components.register("dropdown-filter", {
    viewModel: DropdownFilterViewModel,
    template: DropdownFilterTemplate
});
    