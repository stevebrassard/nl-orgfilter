import ko from 'knockout';
import ServiceDropdownViewModel from '../ViewModels/ServiceDropdownViewModel';
import DropdownFilterTemplate from '../Templates/DropdownFilterTemplate.html';
import '../Styles/DropdownFilter.css'

export default ko.components.register("service-dropdown", {
    viewModel: ServiceDropdownViewModel,
    template: DropdownFilterTemplate
});
    